#import "RNHasInternet.h"
#import <arpa/inet.h>

@implementation RNHasInternet

RCT_EXPORT_MODULE(HasInternetModule)

-(BOOL)isReachableHost:(NSString*)name {
    const char * _name = [name cStringUsingEncoding:NSASCIIStringEncoding];
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, _name);
    SCNetworkReachabilityFlags flags;
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(reachability, &flags);
    CFRelease(reachability);
    if (!didRetrieveFlags) {
        return NO;
    } else {
        BOOL isReachable = flags & kSCNetworkFlagsReachable;
        BOOL isLAN = flags & kSCNetworkReachabilityFlagsIsLocalAddress;
        BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
        if (isReachable && !isLAN && !needsConnection) {
            return YES;
        } else {
            return NO;
        }
    }
}

-(BOOL)isReachableIP:(NSString*)ip {
    const char * _ip = [ip cStringUsingEncoding:NSASCIIStringEncoding];
    struct sockaddr_in sin;
    memset(&sin, 0, sizeof(sin));
    sin.sin_len = sizeof(sin);
    sin.sin_family = AF_INET;
    sin.sin_port = htons(53);
    sin.sin_addr.s_addr = inet_addr(_ip);
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (struct sockaddr *)&sin);
    SCNetworkReachabilityFlags flags;
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(reachability, &flags);
    CFRelease(reachability);
    if (!didRetrieveFlags) {
        return NO;
    } else {
        BOOL isReachable = flags & kSCNetworkFlagsReachable;
        BOOL isLAN = flags & kSCNetworkReachabilityFlagsIsLocalAddress;
        BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
        if (isReachable && !isLAN && !needsConnection) {
            return YES;
        } else {
            return NO;
        }
    }
}

RCT_REMAP_METHOD(isConnected,
                 isConnectedWithResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
    if ([self isReachableIP:@"1.1.1.1"]) {
        resolve([NSNumber numberWithBool:YES]);
    } else if ([self isReachableIP:@"1.0.0.1"]) {
        resolve([NSNumber numberWithBool:YES]);
    } else if ([self isReachableHost:@"wikipedia.org"]) {
        resolve([NSNumber numberWithBool:YES]);
    } else if ([self isReachableHost:@"ietf.org"]) {
        resolve([NSNumber numberWithBool:YES]);
    } else {
        resolve([NSNumber numberWithBool:NO]);
    }
}

@end
