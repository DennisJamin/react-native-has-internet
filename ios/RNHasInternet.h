#import <Foundation/Foundation.h>
#import <SystemConfiguration/SCNetworkReachability.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTBridge.h>

@interface RNHasInternet : NSObject<RCTBridgeModule>

@end
