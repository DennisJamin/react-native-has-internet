require 'json'
pjson = JSON.parse(File.read('package.json'))

Pod::Spec.new do |s|

  s.name            = "RNHasInternet"
  s.version         = pjson["version"]
  s.homepage        = pjson["homepage"]
  s.summary         = pjson["description"]
  s.license         = pjson["license"]
  s.author          = { "Andre Staltz" => "contact@staltz.com" }

  s.ios.deployment_target = '9.0'

  s.source          = { :git => "https://gitlab.com/staltz/react-native-has-internet", :tag => "v#{s.version}" }
  s.source_files    = 'ios/*.{h,m}'
  s.preserve_paths  = "**/*.js"

  s.dependency 'React'
end
