package com.staltz.reactnativehasinternet;

import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.app.Activity;
import android.os.AsyncTask;

import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import android.util.Log;

public class HasInternetModule extends ReactContextBaseJavaModule {

    @Override
    public String getName() {
        return "HasInternetModule";
    }

    public HasInternetModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    private class CheckInet extends AsyncTask<Void, Void, Boolean> {
        public Promise promise = null;

        public CheckInet(Promise promise) {
            this.promise = promise;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if (pingDnsService("1.1.1.1"))
                return true;
            if (pingDnsService("1.0.0.1"))
                return true;
            if (pingHttpServer("http://www.wikipedia.org"))
                return true;
            if (pingHttpServer("http://www.ietf.org"))
                return true;
            return false;
        }

        private Boolean pingDnsService(String ip) {
            try {
                int timeoutMs = 1500;
                Socket sock = new Socket();
                SocketAddress sockaddr = new InetSocketAddress(ip, 53);
                sock.connect(sockaddr, timeoutMs);
                sock.close();
                return true;
            } catch (IOException e) {
                return false;
            }
        }

        private Boolean pingHttpServer(String url) {
            try {
                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("User-Agent", "Mozilla/5.0");
                int responseCode = con.getResponseCode();
                if (responseCode >= 200 && responseCode < 400) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            this.promise.resolve(result);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    @ReactMethod
    public void isConnected(Promise promise) {
        new CheckInet(promise).execute();
    }
}
