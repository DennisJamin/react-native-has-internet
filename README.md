# React Native HasInternet

Simple JS API that answers whether you are connected to the internet or not. Works on both Android and iOS.

The way it works is by attempting to connect with DNS services and popular non-commercial HTTP servers. It operates with a few fallbacks:

1. Attempts to open a DNS socket with `1.1.1.1` (highly available privacy-aware non-Google [DNS service by Cloudfare and APNIC](https://1.1.1.1/))
2. If the above fails, attempts to open a DNS socket with `1.0.0.1` (alternative IP address to the same `1.1.1.1` service)
3. If the above fails, makes an HTTP request to `www.wikipedia.org`
4. If the above fails, makes an HTTP request to `www.ietf.org`
5. If the above fails, declare no internet connection

On iOS, these connections might not be actually established because it uses the [Reachability](https://developer.apple.com/documentation/systemconfiguration/scnetworkreachability-g7d#overview) API.

## Usage

```js
import HasInternet from 'react-native-has-internet';

HasInternet.isConnected().then(isConnected => {
  console.log('Do we have internet connection? ' + isConnected ? 'yes!' : 'no');
});
```

## Installation

## Android

- For React Native 0.60 or higher, use this package's version 4.0.0 or higher
- For React Native 0.59 or lower, use this package's version 3.1.0 or lower

- Run `npm install react-native-has-internet --save` to install using npm.

- Add the following two lines to `android/settings.gradle`:

```gradle
include ':react-native-has-internet'
project(':react-native-has-internet').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-has-internet/android')
```

- Edit `android/app/build.gradle` and add the annoated lines as below:

```gradle
...

dependencies {
    implementation fileTree(dir: "libs", include: ["*.jar"])
    implementation "com.android.support:appcompat-v7:23.0.1"
    implementation "com.facebook.react:react-native:0.55.+"
    implementation project(':react-native-has-internet')  // <- Add this line
}
```

- Edit `android/app/src/main/AndroidManifest.xml` and add the annoated lines as below:

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.reactnativeproject">

    <uses-permission android:name="android.permission.INTERNET" />   <!-- <- Add this line -->

    <application
      android:allowBackup="true"
      android:label="@string/app_name"
      android:icon="@mipmap/ic_launcher"
      android:theme="@style/AppTheme">

    ...
</manifest>
```

## iOS

Update the Podfile:

```diff
+  pod 'RNHasInternet', :path => '../node_modules/react-native-has-internet'
```

Then `cd` into the folder `ios` for your project, and run `pod install` or `pod update`.
